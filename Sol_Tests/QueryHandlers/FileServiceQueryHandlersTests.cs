﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.FileProviders;
using Microsoft.VisualBasic;
using NSubstitute;
using NUnit.Framework;
using NUnit.Framework.Internal;
using TG_Solution.App.Common.Results;
using TG_Solution.App.Queries;
using TG_Solution.App.QueryHandlers;
using TG_Solution.App.Services;

namespace Sol_Tests.QueryHandlers
{
    [TestFixture]
    public class FileServiceQueryHandlersTests
    {
        private string invalidPyr = $"10{Environment.NewLine}20 abc";
        private string validPyr = $"10{Environment.NewLine}20 30";

        private IFsService GetFsThatReturnFile(string fileContent)
        {
            var fs = Substitute.For<IFsService>();
            fs.GetFileContent(Arg.Any<string>()).Returns(PayloadedResultBase<string>.OkWithResult(fileContent));
            return fs;
        }

        private FileServiceQueryHandlers GetHandlerWithFileContent(string fileContent) => new FileServiceQueryHandlers(GetFsThatReturnFile(fileContent));

        [Test]
        public async Task test_null_file() => await AssertFileExpectedWithContent(null);

        [Test]
        public async Task test_empty_file() => await AssertFileExpectedWithContent("");

        [Test]
        public async Task test_string() => await AssertFileExpectedWithContent("string content");

        [Test]
        public async Task test_int_string_mixed() => await AssertFileExpectedWithContent("10 string");

        [Test]
        public async Task test_invalid_pyr() => await AssertFileExpectedWithContent(invalidPyr);

        [Test]
        public async Task test_valid_pyr() => await AssertFileExpectedWithContent(validPyr, true);

        private async Task AssertFileExpectedWithContent(string fileContent, bool expected = false)
        {
            var handler = GetHandlerWithFileContent(fileContent);
            var content = await handler.Handle(new QueryFileNumbers(""));
            Assert.That(content.Success == expected);
        }
    }
}