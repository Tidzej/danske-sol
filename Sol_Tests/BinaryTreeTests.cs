﻿using NUnit.Framework;
using TG_Solution.DataStructures;

namespace Sol_Tests
{
    [TestFixture]
    public class BinaryTreeTests
    {
        [SetUp]
        public void Setup()
        {
            int[] numers = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
            _tree = new BinaryTree(numers);
        }

        private BinaryTree _tree;

        private void TestNode(Node node, int? expectedLeft, int? expectedRight)
        {
            Assert.That(node?.Left?.Value == expectedLeft);
            Assert.That(node?.Right?.Value == expectedRight);
        }

        [Test]
        public void test_only_root()
        {
            var exp = 0;
            _tree = new BinaryTree(new[] {exp});
            Assert.AreEqual(exp, _tree.Root.Value);
        }

        [Test]
        public void test_empty()
        {
            _tree = new BinaryTree(new int[] { });
            Assert.IsNull(_tree.Root);
        }

        [Test]
        public void test_null()
        {
            _tree = new BinaryTree(null);
            Assert.IsNull(_tree.Root);
        }


        //0
        //1  2
        //3  4  5
        //6  7  8  9
        //10 11 12 13 14
        [Test]
        public void test_first_three_rows()
        {
            TestNode(_tree.Root, 1, 2);
            TestNode(_tree.Root.Left, 3, 4);
            TestNode(_tree.Root.Right, null, null);
            TestNode(_tree.Root.Left.Left, null, null);
            TestNode(_tree.Root.Left.Right, 7, 8);
            TestNode(_tree.Root.Right.Left, null, null);
            TestNode(_tree.Root.Right.Right, null, null);
        }

        [Test]
        public void TestValidPathCount()
        {
            //0 1 4 7 12
            Assert.That(_tree.Paths.Count == 1);
        }
    }
}