﻿using System;
using System.Threading.Tasks;
using TG_Solution.App.Commands;
using TG_Solution.DependencyInjection.Attributes;
using TG_Solution.Infrastructure.Handlers;

namespace TG_Solution.App.CommandHandlers
{
    [InjectService]
    public class DisplayCommandHandlers
        : ICommandHandler<ConsoleDisplayAnswerCommand>
    {
        public Task Handle(ConsoleDisplayAnswerCommand command)
        {
            Console.WriteLine(command.Message);
            return Task.CompletedTask;
        }
    }
}