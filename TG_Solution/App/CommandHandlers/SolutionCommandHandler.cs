﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TG_Solution.App.Commands;
using TG_Solution.App.Commands.Results;
using TG_Solution.App.Common;
using TG_Solution.App.Common.Results;
using TG_Solution.App.Queries;
using TG_Solution.DataStructures;
using TG_Solution.DependencyInjection.Attributes;
using TG_Solution.Infrastructure.Buses;
using TG_Solution.Infrastructure.Factories;
using TG_Solution.Infrastructure.Handlers;

namespace TG_Solution.App.CommandHandlers
{
    [InjectService]
    public class SolutionCommandHandler
        : ICommandHandler<CreateSolutionCommand, PayloadedResultBase<string>>
    {
        public async Task<PayloadedResultBase<string>> Handle(CreateSolutionCommand command)
        {
            var iSolution = SolutionFactory.Create(command.SolutionInfo);
            var answer = iSolution.GetAnswer();
            return PayloadedResultBase<string>.OkWithResult(answer);
        }
    }
}