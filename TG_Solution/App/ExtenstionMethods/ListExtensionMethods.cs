﻿using System.Collections.Generic;

namespace TG_Solution.App.ExtenstionMethods
{
    public static class ListExtensionMethods
    {
        public static string ListToString(this IEnumerable<int> list) => string.Join(',', list);
    }
}