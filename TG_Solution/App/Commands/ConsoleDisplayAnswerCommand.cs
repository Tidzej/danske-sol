﻿using TG_Solution.App.Commands.Results;
using TG_Solution.Infrastructure.Commands;

namespace TG_Solution.App.Commands
{
    public class ConsoleDisplayAnswerCommand : ICommand
    {
        public string Message { get; }
        public ConsoleDisplayAnswerCommand(string message) { Message = message; }
    }
}