﻿using TG_Solution.App.Commands.Results;
using TG_Solution.App.Common.Results;
using TG_Solution.App.Solutions;
using TG_Solution.Infrastructure.Commands;
using TG_Solution.Infrastructure.Factories;

namespace TG_Solution.App.Commands
{
    public class CreateSolutionCommand : ICommand<PayloadedResultBase<string>>
    {
        public CreateSolutionCommand(SolutionInfo<ISolution> solutionInfo) { SolutionInfo = solutionInfo; }
        public SolutionInfo<ISolution> SolutionInfo { get; }
    }
}