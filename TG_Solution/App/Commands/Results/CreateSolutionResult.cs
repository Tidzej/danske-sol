﻿namespace TG_Solution.App.Commands.Results
{
    public class CreateSolutionResult
    {
        public CreateSolutionResult(string answer) { Answer = answer; }
        public string Answer { get; }
    }
}