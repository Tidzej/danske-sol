﻿using System.Threading.Tasks;
using TG_Solution.App.Commands;
using TG_Solution.App.Commands.Results;
using TG_Solution.App.Common;
using TG_Solution.App.Common.Results;
using TG_Solution.App.Queries;
using TG_Solution.DependencyInjection.Attributes;
using TG_Solution.Infrastructure.Buses;
using TG_Solution.Infrastructure.Factories;

namespace TG_Solution.App
{
    [InjectService]
    public class Core
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryProcessor _queryProcessor;

        public Core(ICommandBus commandBus, IQueryProcessor queryProcessor)
        {
            _commandBus = commandBus;
            _queryProcessor = queryProcessor;
        }

        public async Task DisplaySolution()
        {
            var fileContent = await _queryProcessor.Process<QueryFileNumbers, PayloadedResultBase<int[]>>(new QueryFileNumbers(MagicStrings.SOLUTION_INPUT_FILE_NAME));
            if (!fileContent.Success)
            {
                await _commandBus.SendAsync(new ConsoleDisplayAnswerCommand(fileContent.Message));
                return;
            }

            var answer = await _commandBus.SendAsync<CreateSolutionCommand, PayloadedResultBase<string>>(new CreateSolutionCommand(new DanskeSolutionInfo(fileContent.Payload)));
            if (!answer.Success) return;

            await _commandBus.SendAsync(new ConsoleDisplayAnswerCommand(answer.Payload));
        }
    }
}