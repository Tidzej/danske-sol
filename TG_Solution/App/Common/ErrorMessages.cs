﻿namespace TG_Solution.App.Common
{
    public static class ErrorMessages
    {
        public const string FILE_PATH_NULL = "Path cannot be null";
        public const string FILE_IS_DIRECTORY = "Provided path is not file";
        public const string FILE_NOT_EXISTS = "File not exists";
        public const string FILE_CONTENT_EMPTY = "File content is empty";
        public const string FILE_CONTENT_INVALID = "File content is invalid. File should contain only numbers separated by space";
        public const string FILE_CONTENT_NOT_PYRAMID = "File content is not valid pyramid";
    }
}