﻿namespace TG_Solution.App.Common.Results
{
    public class ResultBase
    {
        public ResultBase(string message, bool success)
        {
            Message = message;
            Success = success;
        }
        public string Message { get; }
        public bool Success { get; }
    }
}