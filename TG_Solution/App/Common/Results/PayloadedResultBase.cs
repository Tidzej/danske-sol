﻿namespace TG_Solution.App.Common.Results
{
    public class PayloadedResultBase<T> : ResultBase where T : class
    {
        public static PayloadedResultBase<T> Ok() => new PayloadedResultBase<T>(null, true, null);
        public static PayloadedResultBase<T> OkWithResult(T payload) => new PayloadedResultBase<T>(null, true, payload);
        public static PayloadedResultBase<T> Fail(string message) => new PayloadedResultBase<T>(message, false, null);
        public static PayloadedResultBase<T> FailWithResult(string message, T payload) => new PayloadedResultBase<T>(message, false, payload);


        public PayloadedResultBase(string message, bool success, T payload) : base(message, success) { Payload = payload; }
        public T Payload { get; }
    }
}