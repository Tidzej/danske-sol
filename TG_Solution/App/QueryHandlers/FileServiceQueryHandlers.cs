﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.FileProviders;
using TG_Solution.App.Common;
using TG_Solution.App.Common.Results;
using TG_Solution.App.Queries;
using TG_Solution.App.Services;
using TG_Solution.DependencyInjection.Attributes;
using TG_Solution.Infrastructure.Handlers;

namespace TG_Solution.App.QueryHandlers
{
    [InjectService]
    public class FileServiceQueryHandlers
        : IQueryHandler<QueryFileNumbers, PayloadedResultBase<int[]>>
    {
        private readonly IFsService _fsService;
        public FileServiceQueryHandlers(IFsService fsService) => _fsService = fsService;

        public async Task<PayloadedResultBase<int[]>> Handle(QueryFileNumbers query)
        {
            var fileContentResult = await _fsService.GetFileContent(query.FileName);
            if (!fileContentResult.Success) return PayloadedResultBase<int[]>.Fail(fileContentResult.Message);
            if (string.IsNullOrWhiteSpace(fileContentResult.Payload)) return PayloadedResultBase<int[]>.Fail(ErrorMessages.FILE_CONTENT_EMPTY);

            var lineSplited = new FileContentTransform().FileContentToValidPyramid(fileContentResult.Payload);
            if (!lineSplited.Success) return PayloadedResultBase<int[]>.Fail(lineSplited.Message);

            return ConvertStrToIntArray(lineSplited.Payload);
        }

        private PayloadedResultBase<int[]> ConvertStrToIntArray(string[] lineSplited)
        {
            try
            {
                var intArr = Array.ConvertAll(lineSplited, int.Parse);
                return PayloadedResultBase<int[]>.OkWithResult(intArr);
            }
            catch (Exception e)
            {
                return PayloadedResultBase<int[]>.Fail(ErrorMessages.FILE_CONTENT_INVALID);
            }
        }
    }
}