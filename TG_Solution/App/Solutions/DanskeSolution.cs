﻿using System;
using System.Collections.Generic;
using System.Linq;
using TG_Solution.App.ExtenstionMethods;
using TG_Solution.DataStructures;

namespace TG_Solution.App.Solutions
{
    public class DanskeSolution : ISolution
    {
        private readonly int[] _numbers;
        public DanskeSolution(int[] numbers) => _numbers = numbers;

        public string GetAnswer()
        {
            BinaryTree tree = new BinaryTree(_numbers);
            return $"Max sum:{tree.GetLongestPath.Key}{Environment.NewLine}Path:{tree.GetLongestPath.Value.ListToString()}";
        }
    }
}