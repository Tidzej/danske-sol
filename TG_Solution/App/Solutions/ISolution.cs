﻿namespace TG_Solution.App.Solutions
{
    public interface ISolution
    {
        string GetAnswer();
    }
}