﻿using TG_Solution.App.Common.Results;
using TG_Solution.Infrastructure.Commands;

namespace TG_Solution.App.Queries
{
    public class QueryFileNumbers : IQuery<PayloadedResultBase<int[]>>
    {
        public string FileName { get; }
        public QueryFileNumbers(string fileName) { FileName = fileName; }
    }
}