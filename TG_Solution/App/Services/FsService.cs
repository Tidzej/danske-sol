﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.FileProviders;
using Microsoft.VisualBasic;
using TG_Solution.App.Common;
using TG_Solution.App.Common.Results;
using TG_Solution.DependencyInjection.Attributes;

namespace TG_Solution.App.Services
{
    public interface IFsService
    {
        Task<PayloadedResultBase<string>> GetFileContent(string fileName);
    }

    [InjectService]
    public class FsService : IFsService
    {
        private readonly IFileProvider _fileProvider;
        public FsService(IFileProvider fileProvider) => _fileProvider = fileProvider;

        public async Task<PayloadedResultBase<string>> GetFileContent(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName)) return PayloadedResultBase<string>.Fail(ErrorMessages.FILE_PATH_NULL);
            var fileInfo = _fileProvider.GetFileInfo(fileName);
            if (!fileInfo.Exists) return PayloadedResultBase<string>.Fail(ErrorMessages.FILE_NOT_EXISTS);
            if (fileInfo.IsDirectory) return PayloadedResultBase<string>.Fail(ErrorMessages.FILE_IS_DIRECTORY);

            var fileContent = await GetFileContentInternal(fileInfo);

            return PayloadedResultBase<string>.OkWithResult(fileContent);
        }

        private async Task<string> GetFileContentInternal(IFileInfo fileInfo)
        {
            await using var fileStream = fileInfo.CreateReadStream();
            using var fileReader = new StreamReader(fileStream);
            return await fileReader.ReadToEndAsync();
        }
    }
}