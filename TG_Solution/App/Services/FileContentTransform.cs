﻿using System;
using System.Collections.Generic;
using System.Linq;
using TG_Solution.App.Common;
using TG_Solution.App.Common.Results;

namespace TG_Solution.App.Services
{
    public class FileContentTransform
    {
        public PayloadedResultBase<string[]> FileContentToValidPyramid(string fileContent)
        {
            List<string> result = new List<string>();
            var split = fileContent.Split(Environment.NewLine);
            for (var i = 0; i < split.Length; i++)
            {
                var lineSplitted = split[i].Split(" ", StringSplitOptions.RemoveEmptyEntries);
                if (lineSplitted != null && lineSplitted.Length != i + 1) return PayloadedResultBase<string[]>.Fail(ErrorMessages.FILE_CONTENT_NOT_PYRAMID);
                result.AddRange(lineSplitted);
            }

            return PayloadedResultBase<string[]>.OkWithResult(result.ToArray());
        }
    }
}