﻿using System;
using System.ComponentModel.Design;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using TG_Solution.App;
using TG_Solution.DependencyInjection;
using TG_Solution.DependencyInjection.ServiceBuilders;

namespace TG_Solution
{
    static class Program
    {
        static async Task Main(string[] args)
        {
            var serviceProvider = new AutoFacServiceProviderBuilder().Build();
            var solution = serviceProvider.GetRequiredService<Core>();

            await solution.DisplaySolution();

            Console.ReadLine();
        }
    }
}