﻿using System;
using System.IO;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using TG_Solution.DependencyInjection.Attributes;

namespace TG_Solution.DependencyInjection.ServiceBuilders
{
    public class AutoFacServiceProviderBuilder : IServiceProviderBuilder
    {
        public IServiceProvider Build()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.Scan(selector =>
                selector.FromCallingAssembly().AddClasses(
                            filter => filter.WithAttribute<InjectService>())
                        .AsSelfWithInterfaces().WithScopedLifetime());

            IFileProvider fileProvider = new PhysicalFileProvider(Directory.GetCurrentDirectory());

            var containerBuilder = new ContainerBuilder();
            containerBuilder.Register(x => fileProvider);

            containerBuilder.Populate(serviceCollection);
            var container = containerBuilder.Build();

            return new AutofacServiceProvider(container);
        }
    }
}