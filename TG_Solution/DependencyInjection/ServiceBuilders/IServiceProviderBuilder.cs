﻿using System;

namespace TG_Solution.DependencyInjection.ServiceBuilders
{
    public interface IServiceProviderBuilder
    {
        IServiceProvider Build();
    }
}