﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using TG_Solution.DependencyInjection.Attributes;
using TG_Solution.Infrastructure.Commands;
using TG_Solution.Infrastructure.Handlers;

namespace TG_Solution.Infrastructure.Buses
{
    [InjectService]
    public class QueryProcessor : IQueryProcessor
    {
        private readonly IServiceProvider _serviceProvider;
        public QueryProcessor(IServiceProvider serviceProvider) { _serviceProvider = serviceProvider; }

        public Task<TResult> Process<TQuery, TResult>(TQuery query)
            where TQuery : IQuery<TResult>
        {
            var service = _serviceProvider.GetRequiredService<IQueryHandler<TQuery, TResult>>();
            return service.Handle(query);
        }
    }
}