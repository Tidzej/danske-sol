﻿using System.Threading.Tasks;
using TG_Solution.Infrastructure.Commands;

namespace TG_Solution.Infrastructure.Buses
{
    public interface ICommandBus
    {
        Task<TResult> SendAsync<TCommand, TResult>(TCommand command) where TCommand : ICommand<TResult>;
        Task SendAsync<TCommand>(TCommand command) where TCommand : ICommand;
    }
}