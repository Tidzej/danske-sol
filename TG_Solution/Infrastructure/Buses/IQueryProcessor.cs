﻿using System.Threading.Tasks;
using TG_Solution.Infrastructure.Commands;

namespace TG_Solution.Infrastructure.Buses
{
    public interface IQueryProcessor
    {
        Task<TResult> Process<TQuery, TResult>(TQuery query) where TQuery : IQuery<TResult>;
    }
}