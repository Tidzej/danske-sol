﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using TG_Solution.DependencyInjection.Attributes;
using TG_Solution.Infrastructure.Commands;
using TG_Solution.Infrastructure.Handlers;

namespace TG_Solution.Infrastructure.Buses
{
    [InjectService]
    public class CommandBus : ICommandBus
    {
        private readonly IServiceProvider _serviceProvider;
        public CommandBus(IServiceProvider serviceProvider) { _serviceProvider = serviceProvider; }

        public Task SendAsync<TCommand>(TCommand command) where TCommand : ICommand
        {
            var decorator = _serviceProvider.GetService<IDecoratorCommandHandler<TCommand>>();
            var commandHandler = _serviceProvider.GetRequiredService<ICommandHandler<TCommand>>();
            return decorator != null ? decorator.Decorate(command) : commandHandler.Handle(command);
        }

        public Task<TResult> SendAsync<TCommand, TResult>(TCommand command) where TCommand : ICommand<TResult>
        {
            var decorator = _serviceProvider.GetService<IDecoratorCommandHandler<TCommand, TResult>>();
            var commandHandler = _serviceProvider.GetRequiredService<ICommandHandler<TCommand, TResult>>();
            return decorator != null ? decorator.Decorate(command) : commandHandler.Handle(command);
        }
    }
}