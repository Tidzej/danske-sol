﻿namespace TG_Solution.Infrastructure.Commands
{
    public interface ICommand { }

    public interface ICommand<TResponse> : ICommand { }
}