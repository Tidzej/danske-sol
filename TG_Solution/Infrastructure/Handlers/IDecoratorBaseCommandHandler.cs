﻿using System.Threading.Tasks;
using TG_Solution.Infrastructure.Commands;

namespace TG_Solution.Infrastructure.Handlers
{
    public interface IDecoratorCommandHandler<in TCommand> where TCommand : ICommand
    {
        Task Decorate(TCommand command);
    }

    public interface IDecoratorCommandHandler<in TCommand, TResponse> where TCommand : ICommand<TResponse>
    {
        Task<TResponse> Decorate(TCommand command);
    }
}