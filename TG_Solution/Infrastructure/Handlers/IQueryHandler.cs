﻿using System.Threading.Tasks;
using TG_Solution.Infrastructure.Commands;

namespace TG_Solution.Infrastructure.Handlers
{
    public interface IQueryHandler<in TQuery, TResult> where TQuery : IQuery<TResult>
    {
        Task<TResult> Handle(TQuery query);
    }
}