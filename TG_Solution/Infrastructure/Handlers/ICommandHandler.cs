﻿using System.Threading.Tasks;
using TG_Solution.Infrastructure.Commands;

namespace TG_Solution.Infrastructure.Handlers
{
    public interface ICommandHandler<in T> where T : ICommand
    {
        Task Handle(T command);
    }

    public interface ICommandHandler<in TCommand, TResponse> where TCommand : ICommand<TResponse>
    {
        Task<TResponse> Handle(TCommand command);
    }
}