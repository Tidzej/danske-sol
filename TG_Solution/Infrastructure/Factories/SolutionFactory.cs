﻿using TG_Solution.App.Solutions;

namespace TG_Solution.Infrastructure.Factories
{
    public static class SolutionFactory
    {
        public static ISolution Create(SolutionInfo<ISolution> solutionInfo) => solutionInfo.Create();
    }
}