﻿using TG_Solution.App.Solutions;

namespace TG_Solution.Infrastructure.Factories
{
    public abstract class SolutionInfo<T> where T : ISolution
    {
        public abstract T Create();
    }
}