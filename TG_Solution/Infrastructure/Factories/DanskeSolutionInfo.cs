﻿using TG_Solution.App.Solutions;

namespace TG_Solution.Infrastructure.Factories
{
    public class DanskeSolutionInfo : SolutionInfo<ISolution>
    {
        private readonly int[] _numbers;
        public DanskeSolutionInfo(int[] numbers) { _numbers = numbers; }
        public override ISolution Create() => new DanskeSolution(_numbers);
    }
}