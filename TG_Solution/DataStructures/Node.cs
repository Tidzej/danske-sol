﻿namespace TG_Solution.DataStructures
{
    public class Node
    {
        public Node(int value) => Value = value;

        public int Value { get; }
        public Node Left { get; set; }
        public Node Right { get; set; }
    }
}