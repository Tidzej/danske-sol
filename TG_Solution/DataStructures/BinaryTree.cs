﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TG_Solution.App.Commands.Comparers;

namespace TG_Solution.DataStructures
{
    public class BinaryTree
    {
        private readonly int[] _numberLines;
        public readonly Node Root;

        public readonly SortedList<int, IList<int>> Paths = new SortedList<int, IList<int>>(new DuplicateKeyCOmparer<int>());

        public BinaryTree(int[] numberLines)
        {
            if (numberLines == null || numberLines.Length == 0) return;

            _numberLines = numberLines;
            Root = new Node(numberLines.First());
            InsertForNode(Root, 1, 0, new List<int>());
        }

        public KeyValuePair<int, IList<int>> GetLongestPath => Paths.Last();

        private void InsertForNode(Node node, int currentHeight, int lastIndex, IList<int> path)
        {
            path = path.Append(node.Value).ToList();
            var leftIndex = lastIndex + currentHeight;
            var rightIndex = lastIndex + currentHeight + 1;

            if (leftIndex > _numberLines.Length || rightIndex > _numberLines.Length)
                Paths.Add(path.Sum(), path);
            else
            {
                node.Left = new Node(_numberLines[leftIndex]);
                node.Right = new Node(_numberLines[rightIndex]);

                if (!SameParity(node.Value, node.Left.Value)) InsertForNode(node.Left, currentHeight + 1, leftIndex, path);
                if (!SameParity(node.Value, node.Right.Value)) InsertForNode(node.Right, currentHeight + 1, rightIndex, path);
            }
        }

        private bool SameParity(int right, int left) => right % 2 == left % 2;
    }
}